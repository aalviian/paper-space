#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "glm.cpp"


float screenLeft   = -10;
float screenRight  =  10;
float screenTop    =  10;
float screenBottom = -10;

int screenWidth  = 800; //piksel
int screenHeight = 612; //piksel

double posisiX=0, posisiY=0;
double targetX=0, targetY=0;
double mouseX=0, mouseY=0;
double k=0;
double y=-7;
int status=0, status2=0;
int acak1,acak2; //acak1 utk random batang user , acak2 utk random batang computer
float tinggi, speed=0.001;
int wrong=1,benar=0;
int nilai=0,cetaknilai=1;
float transparan=0;

char nama[100];

int acak() { //utk batang komputer
    int z;
    z=-6 + rand() % 14;
    return z;
}
/*int acakan() { //utk batang user
    int z;
    z=-10 + rand() % 14;
    return z;
}*/

int acaktinggi() { //utk acak tinggi batang komputer
    tinggi=rand() % 14;
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch(action){

            case GLFW_PRESS :
            switch(key) {
              case GLFW_KEY_ESCAPE :
                    glfwSetWindowShouldClose(window, GL_TRUE); // Close program when ESC key release
                    break;
                  case GLFW_KEY_UP : //restart atau back to menu utama
                    if(status==0 || status2==0)
                    {
                        status2=1;

                    }
                    else if (status==1) {
                        status2=1;
                    }
                    status=1;
                    break;

                case GLFW_KEY_SPACE : //tombol main atau play game
                    if(status==0){
                        status=1;
                    }
                    else {
                        if(abs(k-tinggi-2)<0.2){
                            benar=1;
                        } else
                            wrong=0;
                    }

                    if(benar){
                        acak1=acak();
                        //acak2=acakan();
                        acaktinggi();
                        k=0;
                        nilai++;
                        benar=0;
                        speed+=0.0005;
                    }
                break;
                case GLFW_KEY_ENTER : //play game atau play again atau restart
                    wrong=1;
                    status=1;
                    speed=0.001;
                    k=0;
                    printf("\n");
                    printf("Masukkan nama kamu: ");
                    gets(nama);
                    cetaknilai=1;
                    y=-7;tinggi=0;
                    benar=1;
                    nilai=0;
                break;


            }
        break;
    }


}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
    switch(action){
        case GLFW_PRESS :
            switch(button) {
                case GLFW_MOUSE_BUTTON_LEFT :
                    targetX=(screenRight-screenLeft)*mouseX/screenWidth + screenLeft;
                    targetY=(screenBottom-screenTop)*mouseY/screenHeight+ screenTop;
                    posisiX=targetX;
                    posisiY=targetY;


                    break;
            }
    }
}

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
    glfwGetCursorPos(window, &xpos, &ypos); //Mendapatkan posisi mouse
    mouseX=xpos;
    mouseY=ypos;
}
void load_BMP_texture(char *filename) {

    FILE *file;
    short int bpp;
    short int planes;
    long size;
    unsigned int texture;

    long imwidth;
    long imheight;
    char *imdata;

    file = fopen(filename, "rb");
    fseek(file, 18, SEEK_CUR);

    fread(&imwidth, 4, 1, file);
    fread(&imheight, 4, 1, file);
    size = imwidth * imheight * 3;

    fread(&bpp, 2, 1, file);
    fread(&planes, 2, 1, file);

    fseek(file, 24, SEEK_CUR);
    imdata = (char *)malloc(size);

    fread(imdata, size, 1, file);

	char temp;
    for(long i = 0; i < size; i+=3){
        temp = imdata[i];
        imdata[i] = imdata[i+2];
        imdata[i+2] = temp;
    }

    fclose(file);

    glGenTextures(1, &texture); // then we need to tell OpenGL that we are generating a texture
    glBindTexture(GL_TEXTURE_2D, texture); // now we bind the texture that we are working with

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, imwidth, imheight, 0, GL_RGB, GL_UNSIGNED_BYTE, imdata);

    free(imdata); // free the texture
}

void calculate(){

     //Tembus
     if(posisiY>screenTop) posisiY=screenBottom;
     if(posisiY<screenBottom) posisiY=screenTop;
     if(posisiX>screenRight) posisiX=screenLeft;
     if(posisiX<screenLeft) posisiX=screenRight;


}

void setup_viewport(GLFWwindow* window)
{
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(screenLeft , screenRight, screenBottom, screenTop, 1.f, -1.f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

}

void grid()
{
     float i;
     glBegin(GL_LINES);
         glLineWidth(1);
         //Menggambar baris vertikal
         for(i=screenLeft;i<=screenRight;i++)
         {
            glVertex2f(i, screenTop);
            glVertex2f(i, screenBottom);
         }

         //Menggambar baris horizontal
         for(i=screenBottom;i<=screenTop;i++)
         {
            glVertex2f(screenLeft, i);
            glVertex2f(screenRight, i);
         }

         glLineWidth(3);
         glColor3f(0.5,0.5,0.5);
         glVertex2f(screenLeft,0);
         glVertex2f(screenRight,0);
         glVertex2f(0, screenBottom);
         glVertex2f(0, screenTop);

     glEnd();
}

void screen() {

    glEnable(GL_TEXTURE_2D);
    load_BMP_texture("gambar game home.bmp");

    glBegin(GL_POLYGON);
        glTexCoord2f(0, 0);
        glVertex3f(-10, -10, 0);
        glTexCoord2f(1, 0);
        glVertex3f(10, -10, 0);
        glTexCoord2f(1, 1);
        glVertex3f(10, 10, 0);
        glTexCoord2f(0, 1);
        glVertex3f(-10, 10, 0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}
void pause() {
    status=0;
    glBegin(GL_POLYGON);
    glColor3ub(46, 204, 113);
        glVertex3f(-10, -10, 0);
        glVertex3f(10, -10, 0);
        glVertex3f(10, 10, 0);
        glVertex3f(-10, 10, 0);
    glEnd();
}
void background()
{

    glBegin(GL_POLYGON);
    glColor3ub(26, 188, 156);
        glVertex3f(-10, -10, 0);
        glVertex3f(10, -10, 0);
        glVertex3f(10, 10, 0);
        glVertex3f(-10, 10, 0);
    glEnd();

}

void square(/*int z*/){
    glBegin(GL_POLYGON);
    glColor3ub(44, 62, 80);
    glVertex2f(-9, -10);
    glVertex2f(-7, -10);
    glVertex2f(-7, y+k);
    glVertex2f(-9, y+k);
    glEnd();
}

void square1(int z){
    glBegin(GL_POLYGON);
    glColor3ub(192, 57, 43);
    glVertex2f(z, -10);
    glVertex2f(z+2, -10);
    glVertex2f(z+2, -5+tinggi);
    glVertex2f(z, -5+tinggi);
    glEnd();
}

void lose()
{
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_POLYGON_SMOOTH);
        glEnable(GL_POINT_SMOOTH);

        glEnable(GL_TEXTURE_2D);
        glBindTexture (GL_TEXTURE_2D, 99);
        glBegin(GL_POLYGON);
        glColor4f(1,1,1,transparan);
        glTexCoord2f(0, 0);
        glVertex3f(-10, -10, 0);
        glTexCoord2f(1, 0);
        glVertex3f(10, -10, 0);
        glTexCoord2f(1, 1);
        glVertex3f(10, 10, 0);
        glTexCoord2f(0, 1);
        glVertex3f(-10, 10, 0);
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_POINT_SMOOTH);
        glDisable(GL_POLYGON_SMOOTH);
        glDisable(GL_BLEND);

    transparan+=0.05;
    glEnable(GL_TEXTURE_2D);
    load_BMP_texture("gambar game over.bmp");

    glBegin(GL_POLYGON);
        glTexCoord2f(0, 0);
        glVertex3f(-10, -10, 0);
        glTexCoord2f(1, 0);
        glVertex3f(10, -10, 0);
        glTexCoord2f(1, 1);
        glVertex3f(10, 10, 0);
        glTexCoord2f(0, 1);
        glVertex3f(-10, 10, 0);
    glEnd();

    glDisable(GL_TEXTURE_2D);
}

void play() {
    background();
    square1(acak1);
    glPushMatrix();
        if(status==1)
        {
            k+=speed;
        }

     square(/*acak2*/);
     glPopMatrix();

     if(y+k>=10) {
        wrong=0;
     }

}

void display()
{

    //int acak1;

     //grid();

        //if(status2==0) {
            //status2=1;
            //acak1=acak();
        //}
    //calculate();
    if(status==1) {
        play();
    }
    else if(status2==1) {
        pause();
    }
    else {
                glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_LINE_SMOOTH);
        glEnable(GL_POLYGON_SMOOTH);
        glEnable(GL_POINT_SMOOTH);


        glColor4f(1,1,1,transparan);
         screen();
        glEnd();

        glDisable(GL_POINT_SMOOTH);
        glDisable(GL_POLYGON_SMOOTH);
        glDisable(GL_BLEND);

    transparan+=0.007;

    }
}

int main(void)
{
    GLFWwindow* window;

    if (!glfwInit()) exit(EXIT_FAILURE);

    window = glfwCreateWindow(screenWidth, screenHeight, "Paper Space", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwSetKeyCallback(window, key_callback); //Tombol Keyboard
    glfwSetMouseButtonCallback(window, mouse_button_callback); //Tombol Mouse
    glfwSetCursorPosCallback(window, cursor_position_callback ); //Posisi Mouse

    while (!glfwWindowShouldClose(window))
    {
        setup_viewport(window);

        if(wrong){
            background();
            display();
        }else {

            lose();
            if(cetaknilai){
            printf("%s's score: %d\n",nama,nilai);
            if (nilai==0)
                printf("Mungkin %s lelah\n", nama);
            else if (nilai>0 && nilai <= 10)
                printf("%s kurang peka deh\n",nama);
            else if (nilai>10 && nilai <= 30)
                printf("%s peka deh\n",nama);
            else if (nilai>30)
                printf("%s Peka Bangeeeeet deh\n",nama);
            cetaknilai=0;
            }
    }

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    exit(EXIT_SUCCESS);
}
